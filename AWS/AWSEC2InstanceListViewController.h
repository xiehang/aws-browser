//
//  AWSEC2InstanceListViewController.h
//  AWS
//
//  Created by Hang Xie on 12-2-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "UIKit/UIKit.h"

@interface AWSEC2InstanceListViewController : UITableViewController

@property (copy, nonatomic) NSDictionary * selection;
@property (weak, nonatomic) id delegate;
@property (weak, nonatomic) IBOutlet UITableView *myTableView;

@end
