//
//  AWSEC2Instance.m
//  AWS
//
//  Created by Hang Xie on 12-2-4.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "AWSEC2Instance.h"
#import "AWSConstants.h"

@implementation AWSEC2Instance

@synthesize tagTable;
@synthesize endpoint;

- (AWSEC2Instance *)initWithEC2Instance:(EC2Instance *)ec2Instance
{
    self.instanceId = ec2Instance.instanceId;
    self.imageId = ec2Instance.imageId;
    self.state = ec2Instance.state;
    self.privateDnsName = ec2Instance.privateDnsName;
    self.publicDnsName = ec2Instance.publicDnsName;
    self.stateTransitionReason = ec2Instance.stateTransitionReason;
    self.keyName = ec2Instance.keyName;
    self.amiLaunchIndex = ec2Instance.amiLaunchIndex;
    self.productCodes = ec2Instance.productCodes;
    self.instanceType = ec2Instance.instanceType;
    self.launchTime = ec2Instance.launchTime;
    self.placement = ec2Instance.placement;
    self.kernelId = ec2Instance.kernelId;
    self.ramdiskId = ec2Instance.ramdiskId;
    self.platform = ec2Instance.platform;
    self.monitoring = ec2Instance.monitoring;
    self.subnetId = ec2Instance.subnetId;
    self.vpcId = ec2Instance.vpcId;
    self.privateIpAddress = ec2Instance.privateIpAddress;
    self.publicIpAddress = ec2Instance.publicIpAddress;
    self.stateReason = ec2Instance.stateReason;
    self.architecture = ec2Instance.architecture;
    self.rootDeviceType = ec2Instance.rootDeviceType;
    self.rootDeviceName = ec2Instance.rootDeviceName;
    self.blockDeviceMappings = ec2Instance.blockDeviceMappings;
    self.virtualizationType = ec2Instance.virtualizationType;
    self.instanceLifecycle = ec2Instance.instanceLifecycle;
    self.spotInstanceRequestId = ec2Instance.spotInstanceRequestId;
    self.license = ec2Instance.license;
    self.clientToken = ec2Instance.clientToken;
    self.tags = ec2Instance.tags;
    self.securityGroups = ec2Instance.securityGroups;
    self.hypervisor = ec2Instance.hypervisor;
    self.endpoint = nil;

    [self loadTagTable];
    return self;
}

- (void)loadTagTable
{
    if (tagTable == nil)
    {
        tagTable = [[NSMutableDictionary alloc] init];
    }
    else
    {
        [tagTable removeAllObjects];
    }
    for (EC2Tag * tag in self.tags)
    {
        [tagTable setObject:tag.value forKey:tag.key];
    }

    // We always need a Name field
    NSString * name = [tagTable objectForKey:@"Name"];
    if ((name == nil) || ([name compare:AWS_EMPTY_STRING] == NSOrderedSame))
    {
        [tagTable setObject:@"(Anonymous)" forKey:@"Name"];
    }
}

@end
