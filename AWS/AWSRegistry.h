//
//  AWSRegistry.h
//  AWS
//
//  Created by Hang Xie on 12-1-27.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

// these keys should not be localized as they are not presented to user
#define AWS_ACCESS_KEY_ID @"AccessKeyID"
#define AWS_SECRET_ACCESS_KEY @"SecretAccessKey"
#define AWS_REGISTRY_FILE_NAME @"aws.keys"

@interface AWSRegistry : NSObject

@property (nonatomic, strong) NSMutableDictionary * dict;

+ (NSString *)registryFileName;
+ (NSString *)REG_ACCESS_KEY;
+ (NSString *)REG_SECRET_KEY;

+ (AWSRegistry *)instance;
+ (Boolean)save;
+ (Boolean)reload;

@end