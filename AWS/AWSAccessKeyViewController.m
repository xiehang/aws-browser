//
//  AWSAccessKeyViewController.m
//  AWS
//
//  Created by Hang Xie on 12-1-26.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "AWSAccessKeyViewController.h"
#import "AWSRegistry.h"

@implementation AWSAccessKeyViewController

@synthesize fieldAccessKeyID;
@synthesize fieldSecretAccessKey;

- (IBAction)rightButtonClicked:(id)sender
{
    // save credentials

    [[[AWSRegistry instance] dict] setValue:[fieldAccessKeyID text] forKey:[AWSRegistry REG_ACCESS_KEY]];
    [[[AWSRegistry instance] dict] setValue:[fieldSecretAccessKey text] forKey:[AWSRegistry REG_SECRET_KEY]];
    [AWSRegistry save];

    // close the page since everything's done
    [self.navigationController popViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.navigationItem.rightBarButtonItem setTitle:NSLocalizedString(@"Save", @"Caption of 'Save' button in credential page")];

    // load credentials for editing
    [fieldAccessKeyID setText:[[[AWSRegistry instance] dict] valueForKey:[AWSRegistry REG_ACCESS_KEY]]];
    [fieldSecretAccessKey setText:[[[AWSRegistry instance] dict] valueForKey:[AWSRegistry REG_SECRET_KEY]]];
}

- (void)viewDidUnload
{
    [self setFieldAccessKeyID:nil];
    [self setFieldSecretAccessKey:nil];
    [super viewDidUnload];

    // Release any retained subviews of the main view.
    self.navigationItem.rightBarButtonItem = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
}

@end
