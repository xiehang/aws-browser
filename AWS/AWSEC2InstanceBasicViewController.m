//
//  AWSEC2InstanceBasicViewController.m
//  AWS
//
//  Created by Hang Xie on 12-2-13.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <AWSEC2/AmazonEC2Client.h>
#import "AWSEC2InstanceBasicViewController.h"
#import "AWSEC2Constants.h"
#import "AWSRegistry.h"

@implementation AWSEC2InstanceBasicViewController

@synthesize selection;
@synthesize labelAMI;
@synthesize labelZone;
@synthesize labelSecurityGroup;
@synthesize labelType;
@synthesize labelState;
@synthesize labelScheduledEvents;
@synthesize labelOwner;
@synthesize labelVPCID;
@synthesize labelSubnetID;
@synthesize labelSourceDestCheck;
@synthesize labelVisualization;
@synthesize labelPlacementGroup;
@synthesize labelReservation;
@synthesize labelRAMDiskID;
@synthesize labelPlatform;
@synthesize labelKeyPairName;
@synthesize labelKernelID;
@synthesize labelMonitoring;
@synthesize labelAMILaunchIndex;
@synthesize labelElasticIP;
@synthesize labelRootDevice;
@synthesize labelRootDeviceType;
@synthesize labelTenancy;
@synthesize labelLifecycle;
@synthesize labelBlockDevices;
@synthesize labelNetworkInterface;
@synthesize labelPublicDNS;
@synthesize labelPrivateDNS;
@synthesize labelLaunchTime;
@synthesize labelStateTransitionReason;
@synthesize labelTerminationProtection;

NSString * instanceName = nil;
NSString * endpoint = nil;

- (void)loadInstance
{
    [self.navigationItem setTitle:instanceName];

    // get credentials - credentials should be useable as we came from bucket listing page
    NSString * accessKey = [[[AWSRegistry instance] dict] valueForKey:[AWSRegistry REG_ACCESS_KEY]];
    NSString * secretKey = [[[AWSRegistry instance] dict] valueForKey:[AWSRegistry REG_SECRET_KEY]];

    @try
    {
        // now let's load objects in the bucket
        AmazonEC2Client * ec2 = [[AmazonEC2Client alloc]
                                 initWithAccessKey:accessKey
                                 withSecretKey:secretKey];
        [ec2 setEndpoint:[@"http://" stringByAppendingString:endpoint]];

        EC2DescribeInstancesRequest * describeInstancesRequest = [[EC2DescribeInstancesRequest alloc] init];
        [describeInstancesRequest addInstanceId:instanceName];

        EC2DescribeInstancesResponse * describeInstanceReponse = [[EC2DescribeInstancesResponse alloc] init];
        describeInstanceReponse = [ec2 describeInstances:describeInstancesRequest];

        if (([[describeInstanceReponse reservations] count] != 1)
            || ([[[[describeInstanceReponse reservations] objectAtIndex:0] instances] count] != 1)) {
            // more than one reservation or instance found, too bad
            UIAlertView * alert = [[UIAlertView alloc]
                                   initWithTitle:@"AWS Exception"
                                   message:@"more than one instance found with same name"
                                   delegate:self
                                   cancelButtonTitle:AWS_TITLE_OK
                                   otherButtonTitles:nil];
            [alert show];
            return;
        }
        EC2Reservation * reservation = [[describeInstanceReponse reservations] objectAtIndex:0];
        EC2Instance * instance = [[reservation instances] objectAtIndex:0];

        [labelAMI setText:[instance imageId]];
        [labelZone setText:[[instance placement] availabilityZone]];
        NSString * groupDescription = [[NSString alloc] initWithString:[[[instance securityGroups] objectAtIndex:0] groupName]];
        for (int index=1; index<[[instance securityGroups] count]; index++) {
            groupDescription = [groupDescription stringByAppendingString:@", "];
            groupDescription = [groupDescription stringByAppendingString:[[[instance securityGroups] objectAtIndex:index] groupName]];
        }
        [labelSecurityGroup setText:groupDescription];
        [labelType setText:[instance instanceType]];
        [labelState setText:[[instance state] name]];
        [labelScheduledEvents setText:@"Uknown"];
        [labelOwner setText:[reservation ownerId]];
        [labelVPCID setText:[instance vpcId]];
        [labelSubnetID setText:[instance subnetId]];
        [labelSourceDestCheck setText:[instance sourceDestCheck] ? @"Yes" : @"No"];
        [labelVisualization setText:[instance virtualizationType]];
        [labelPlacementGroup setText:[[instance placement] groupName]];
        [labelReservation setText:[reservation reservationId]];
        [labelRAMDiskID setText:[instance ramdiskId]];
        [labelPlatform setText:[instance platform]];
        [labelKeyPairName setText:[instance keyName]];
        [labelKernelID setText:[instance kernelId]];
        [labelMonitoring setText:[[instance monitoring] state]];
        [labelAMILaunchIndex setText:[[instance amiLaunchIndex] stringValue]];
        [labelRootDevice setText:[instance rootDeviceName]];
        [labelRootDeviceType setText:[instance rootDeviceType]];
        [labelTenancy setText:@"Unknown"];
        [labelLifecycle setText:[instance instanceLifecycle]];
        groupDescription = [[NSString alloc] initWithString:[[[instance blockDeviceMappings] objectAtIndex:0] deviceName]];
        for (int index=1; index<[[instance blockDeviceMappings] count]; index++) {
            groupDescription = [groupDescription stringByAppendingString:@", "];
            groupDescription = [groupDescription stringByAppendingString:[[[instance blockDeviceMappings] objectAtIndex:index] deviceName]];
        }
        [labelBlockDevices setText:groupDescription];
        [labelNetworkInterface setText:@"Unknown"];
        [labelPublicDNS setText:[instance publicDnsName]];
        [labelPrivateDNS setText:[instance privateDnsName]];
        [labelLaunchTime setText:[[instance launchTime] description]];
        [labelStateTransitionReason setText:[instance stateTransitionReason]];
        [labelTerminationProtection setText:@"Unknown"];

        EC2DescribeAddressesRequest * describeAddressRequest = [[EC2DescribeAddressesRequest alloc] init];
        [describeAddressRequest addFilter:[[EC2Filter alloc] initWithName:@"instance-id" andValues:[[NSMutableArray alloc] initWithObjects:instanceName,nil]]];
        EC2DescribeAddressesResponse * describeAddressResponse = [ec2 describeAddresses:describeAddressRequest];
        if ([[describeAddressResponse addresses] count] == 0) {
            [labelElasticIP setText:@"None"];
        } else {
            groupDescription = [[NSString alloc] initWithString:[[describeAddressResponse addressesObjectAtIndex:0] description]];
            for (int index=1; index<[[describeAddressResponse addresses] count]; index++) {
                groupDescription = [groupDescription stringByAppendingString:@", "];
                groupDescription = [groupDescription stringByAppendingString:[[describeAddressResponse addressesObjectAtIndex:0] description]];
            }
            [labelElasticIP setText:groupDescription];
        }
    }
    @catch (AmazonClientException *exception)
    {
        UIAlertView * alert = [[UIAlertView alloc]
                               initWithTitle:@"AWS Exception"
                               message:[exception message]
                               delegate:self
                               cancelButtonTitle:AWS_TITLE_OK
                               otherButtonTitles:nil];
        [alert show];
    }
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    instanceName = [[NSString alloc] initWithString:[selection objectForKey:AWS_DATA_KEY_INSTANCE]];
    endpoint = [[NSString alloc] initWithString:[selection objectForKey:AWS_DATA_KEY_ENDPOINT]];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [self setLabelAMI:nil];
    [self setLabelZone:nil];
    [self setLabelSecurityGroup:nil];
    [self setLabelType:nil];
    [self setLabelState:nil];
    [self setLabelScheduledEvents:nil];
    [self setLabelOwner:nil];
    [self setLabelVPCID:nil];
    [self setLabelSubnetID:nil];
    [self setLabelSourceDestCheck:nil];
    [self setLabelVisualization:nil];
    [self setLabelPlacementGroup:nil];
    [self setLabelReservation:nil];
    [self setLabelRAMDiskID:nil];
    [self setLabelPlatform:nil];
    [self setLabelKeyPairName:nil];
    [self setLabelKernelID:nil];
    [self setLabelMonitoring:nil];
    [self setLabelAMILaunchIndex:nil];
    [self setLabelElasticIP:nil];
    [self setLabelRootDevice:nil];
    [self setLabelRootDeviceType:nil];
    [self setLabelTenancy:nil];
    [self setLabelLifecycle:nil];
    [self setLabelBlockDevices:nil];
    [self setLabelNetworkInterface:nil];
    [self setLabelPublicDNS:nil];
    [self setLabelPrivateDNS:nil];
    [self setLabelLaunchTime:nil];
    [self setLabelStateTransitionReason:nil];
    [self setLabelTerminationProtection:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self loadInstance];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 31;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
