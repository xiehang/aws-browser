//
//  AWSS3ObjectListViewController.m
//  AWS
//
//  Created by Hang Xie on 12-1-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <AWSS3/AmazonS3Client.h>
#import "AWSS3ObjectListViewController.h"
#import "AWSS3ObjectBasicViewController.h"
#import "AWSRegistry.h"
#import "AWSS3Constants.h"

@implementation AWSS3ObjectListViewController

@synthesize selection;
@synthesize delegate;
@synthesize myTableView;

NSString * bucketName = nil;
NSString * pathPrefix = nil;

- (void)loadObjects
{
    // clean up existing items
    if (self.items == nil)
    {
        self.items = [[NSMutableArray alloc] init];
    }
    else
    {
        [self.items removeAllObjects];
    }

    if ([pathPrefix length] == 0 || [pathPrefix isEqualToString:AWS_S3_PATH_SEPARATOR])
    {
        [self.navigationItem setTitle:bucketName];
    }
    else
    {
        NSMutableArray * pathComponents = [[NSMutableArray alloc]
                                           initWithArray:[pathPrefix pathComponents]];
        // remove the tailing "/"
        [pathComponents removeLastObject];
        [self.navigationItem setTitle:[pathComponents lastObject]];
    }

    // get credentials - credentials should be useable as we came from bucket listing page
    NSString * accessKey = [[[AWSRegistry instance] dict] valueForKey:[AWSRegistry REG_ACCESS_KEY]];
    NSString * secretKey = [[[AWSRegistry instance] dict] valueForKey:[AWSRegistry REG_SECRET_KEY]];

    @try
    {
        // now let's load objects in the bucket
        AmazonS3Client * s3 = [[AmazonS3Client alloc]
                               initWithAccessKey:accessKey
                               withSecretKey:secretKey];

        // list all "folders"
        S3ListObjectsRequest * listObjectRequest = [[S3ListObjectsRequest alloc] initWithName:bucketName];
        [listObjectRequest setPrefix:pathPrefix];
        [listObjectRequest setDelimiter:AWS_S3_PATH_SEPARATOR];

        S3ListObjectsResponse * listObjectResponse = [s3 listObjects:listObjectRequest];
        S3ListObjectsResult * listObjectsResults = listObjectResponse.listObjectsResult;

        NSMutableDictionary * objectList = [[NSMutableDictionary alloc] init];
        for (NSString * directoryName in listObjectsResults.commonPrefixes)
        {
            NSString * relativePath = [directoryName substringFromIndex:[pathPrefix length]];
            [objectList setObject:[@"\0" stringByAppendingString:relativePath] forKey:relativePath];
        }

        // list all "files"
        for (S3ObjectSummary * objectSummary in listObjectsResults.objectSummaries)
        {
            NSString * objectName = [[NSString alloc] initWithString:[[objectSummary key] substringFromIndex:[pathPrefix length]]];

            if ([objectName length] == 0)
            {
                continue;
            }
            [objectList setObject:objectName forKey:objectName];
        }
        [self.items setArray:[objectList keysSortedByValueUsingSelector:@selector(compare:)]];
    }
    @catch (AmazonClientException *exception)
    {
        UIAlertView * alert = [[UIAlertView alloc]
                               initWithTitle:@"AWS Exception"
                               message:[exception message]
                               delegate:self
                               cancelButtonTitle:AWS_TITLE_OK
                               otherButtonTitles:nil];
        [alert show];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

- (IBAction)rightButtonClicked:(id)sender
{
    // deal with "Up" action, which means moves up
    if ([pathPrefix compare:AWS_EMPTY_STRING] == NSOrderedSame)
    {
        // top level already, return to bucket view
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }

    // go one level up
    NSMutableArray * pathComponents = [[NSMutableArray alloc]
                                       initWithArray:[pathPrefix pathComponents]];
    NSString * tail = [pathComponents lastObject];
    if ([tail compare:AWS_S3_PATH_SEPARATOR] == NSOrderedSame)
    {
        [pathComponents removeLastObject];
    }

    // remove the lowest directory
    [pathComponents removeLastObject];

    if ([pathComponents count] != 0)
    {
        pathPrefix = [[NSString pathWithComponents:pathComponents] stringByAppendingString:AWS_S3_PATH_SEPARATOR];
    }
    else
    {
        pathPrefix = AWS_EMPTY_STRING;
    }
    [self loadObjects];
    [[self myTableView] reloadData];

    return;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.navigationItem.rightBarButtonItem setTitle:@"Up"];
    bucketName = [[NSString alloc] initWithString:[selection objectForKey:AWS_DATA_KEY_BUCKET]];
    pathPrefix = [[NSString alloc] initWithString:[selection objectForKey:AWS_DATA_KEY_PREFIX]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    // normalize prefix
    // - root is "", NOT "/" or nil
    // - others should be ended with a "/"
    if ((pathPrefix == nil) || ([pathPrefix compare:AWS_S3_PATH_SEPARATOR] == NSOrderedSame))
    {
        pathPrefix = AWS_EMPTY_STRING;
    }
    else if (([pathPrefix length] != 0) && (![pathPrefix hasSuffix:AWS_S3_PATH_SEPARATOR]))
    {
        pathPrefix = [pathPrefix stringByAppendingString:AWS_S3_PATH_SEPARATOR];
    }

    // load objects from this bucket
    [self loadObjects];
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
}

// perform different segue based on type of the cell
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];

    NSString * objectName = [self.items objectAtIndex:indexPath.row];
    NSString * fullPath = [pathPrefix stringByAppendingString:objectName];

    if ([fullPath hasSuffix:AWS_S3_PATH_SEPARATOR])
    {
        // this is a directory
        pathPrefix = fullPath;
        [self loadObjects];
        [[self myTableView] reloadData];
    }
    else
    {
        // this is an object
        AWSS3ObjectBasicViewController * objectBasicView = [self.storyboard instantiateViewControllerWithIdentifier:AWS_VIEW_S3_OBJECT_BASIC];

        NSDictionary * objectAttributes =[NSDictionary dictionaryWithObjectsAndKeys:
                                          bucketName, AWS_DATA_KEY_BUCKET,
                                          fullPath, AWS_DATA_KEY_OBJECT,
                                          nil];
        [objectBasicView setValue:objectAttributes forKey:@"selection"];
        [self.navigationController pushViewController:objectBasicView animated:YES];
    }
}

@end
