//
//  AWSViewController.h
//  AWS
//
//  Created by Hang Xie on 12-1-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AWSViewController : UIViewController

- (IBAction)backgroundTouched:(id)sender;
- (IBAction)rightButtonClicked:(id)sender;

@end
