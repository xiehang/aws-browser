//
//  AWSEC2Constants.h
//  AWS
//
//  Created by Hang Xie on 12-2-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#ifndef AWS_AWSEC2Constants_h
#define AWS_AWSEC2Constants_h

#import "AWSConstants.h"

// keys for transferring data between views
#define AWS_DATA_KEY_INSTANCE @"instanceName"
#define AWS_DATA_KEY_ENDPOINT @"endpoint"

#endif
