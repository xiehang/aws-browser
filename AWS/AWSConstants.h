//
//  AWSConstants.h
//  AWS
//
//  Created by Hang Xie on 12-2-2.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#ifndef AWS_AWSConstants_h
#define AWS_AWSConstants_h

#define AWS_EMPTY_STRING @""

// ids for view controllers
#define AWS_VIEW_KEY_ACCESS_KEY   @"UIViewController-uVG-tZ-ufI"

#define AWS_TITLE_OK     NSLocalizedString(@"OK",     @"OK as caption/title")
#define AWS_TITLE_CANCEL NSLocalizedString(@"Cancle", @"Cancle as caption/title")
#define AWS_TITLE_YES    NSLocalizedString(@"Yes",    @"Yes as caption/title")
#define AWS_TITLE_NO     NSLocalizedString(@"No",     @"No as caption/title")

#define AWS_TITLE_RIGHT_BUTTON NSLocalizedString(@"Right Button", @"Default caption of right button in navigation bar, should never be shown")

#define AWS_TITLE_UNKNOWN_ERROR   NSLocalizedString(@"Unknown Error", @"Title of alert window telling unknown error")
#define AWS_MESSAGE_UNKNOWN_ERROR NSLocalizedString(@"Unknown error happened", @"Message telling this is an unknown error")

#define AWS_TITLE_NEED_CREDENTIALS   NSLocalizedString(@"Need AWS Credentials", @"Message to inform user of missing AWS credentials")
#define AWS_MESSAGE_NEED_CREDENTIALS NSLocalizedString(@"Do you want to configure credentials now?", @"Question to ask user if want to input AWS credentials now or not")

#endif
