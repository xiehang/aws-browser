//
//  AWSEC2InstanceBasicViewController.h
//  AWS
//
//  Created by Hang Xie on 12-2-13.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AWSEC2InstanceBasicViewController : UITableViewController

@property (copy, nonatomic) NSDictionary * selection;

@property (weak, nonatomic) IBOutlet UILabel *labelAMI;
@property (weak, nonatomic) IBOutlet UILabel *labelZone;
@property (weak, nonatomic) IBOutlet UILabel *labelSecurityGroup;
@property (weak, nonatomic) IBOutlet UILabel *labelType;
@property (weak, nonatomic) IBOutlet UILabel *labelState;
@property (weak, nonatomic) IBOutlet UILabel *labelScheduledEvents;
@property (weak, nonatomic) IBOutlet UILabel *labelOwner;
@property (weak, nonatomic) IBOutlet UILabel *labelVPCID;
@property (weak, nonatomic) IBOutlet UILabel *labelSubnetID;
@property (weak, nonatomic) IBOutlet UILabel *labelSourceDestCheck;
@property (weak, nonatomic) IBOutlet UILabel *labelVisualization;
@property (weak, nonatomic) IBOutlet UILabel *labelPlacementGroup;
@property (weak, nonatomic) IBOutlet UILabel *labelReservation;
@property (weak, nonatomic) IBOutlet UILabel *labelRAMDiskID;
@property (weak, nonatomic) IBOutlet UILabel *labelPlatform;
@property (weak, nonatomic) IBOutlet UILabel *labelKeyPairName;
@property (weak, nonatomic) IBOutlet UILabel *labelKernelID;
@property (weak, nonatomic) IBOutlet UILabel *labelMonitoring;
@property (weak, nonatomic) IBOutlet UILabel *labelAMILaunchIndex;
@property (weak, nonatomic) IBOutlet UILabel *labelElasticIP;
@property (weak, nonatomic) IBOutlet UILabel *labelRootDevice;
@property (weak, nonatomic) IBOutlet UILabel *labelRootDeviceType;
@property (weak, nonatomic) IBOutlet UILabel *labelTenancy;
@property (weak, nonatomic) IBOutlet UILabel *labelLifecycle;
@property (weak, nonatomic) IBOutlet UILabel *labelBlockDevices;
@property (weak, nonatomic) IBOutlet UILabel *labelNetworkInterface;
@property (weak, nonatomic) IBOutlet UILabel *labelPublicDNS;
@property (weak, nonatomic) IBOutlet UILabel *labelPrivateDNS;
@property (weak, nonatomic) IBOutlet UILabel *labelLaunchTime;
@property (weak, nonatomic) IBOutlet UILabel *labelStateTransitionReason;
@property (weak, nonatomic) IBOutlet UILabel *labelTerminationProtection;

@end
