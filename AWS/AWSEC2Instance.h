//
//  AWSEC2Instance.h
//  AWS
//
//  Created by Hang Xie on 12-2-4.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <AWSEC2/EC2Instance.h>

@interface AWSEC2Instance : EC2Instance

@property (nonatomic, retain) NSMutableDictionary * tagTable;
@property (nonatomic, retain) NSString * endpoint;

- (AWSEC2Instance *)initWithEC2Instance:(EC2Instance *)ec2Instance;
- (void)loadTagTable;

@end
