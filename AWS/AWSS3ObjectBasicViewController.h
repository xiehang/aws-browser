//
//  AWSS3ObjectBasicViewController.h
//  AWS
//
//  Created by Hang Xie on 12-1-31.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "AWSTableViewController.h"

@interface AWSS3ObjectBasicViewController : UITableViewController

@property (copy, nonatomic) NSDictionary * selection;

@property (weak, nonatomic) IBOutlet UILabel *labelBucket;
@property (weak, nonatomic) IBOutlet UILabel *labelPath;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *labelSize;
@property (weak, nonatomic) IBOutlet UILabel *labelLastModified;
@property (weak, nonatomic) IBOutlet UILabel *labelETag;
@property (weak, nonatomic) IBOutlet UILabel *labelExpiryDate;
@property (weak, nonatomic) IBOutlet UILabel *labelLifecycleRule;

@end
