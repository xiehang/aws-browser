//
//  AWSS3ObjectListViewController.h
//  AWS
//
//  Created by Hang Xie on 12-1-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "AWSTableViewController.h"

@interface AWSS3ObjectListViewController : AWSTableViewController

@property (copy, nonatomic) NSDictionary * selection;
@property (weak, nonatomic) id delegate;
@property (weak, nonatomic) IBOutlet UITableView *myTableView;

@end
