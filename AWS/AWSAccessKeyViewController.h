//
//  AWSAccessKeyViewController.h
//  AWS
//
//  Created by Hang Xie on 12-1-26.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AWSViewController.h"

@interface AWSAccessKeyViewController : AWSViewController

@property (weak, nonatomic) IBOutlet UITextField *fieldAccessKeyID;
@property (weak, nonatomic) IBOutlet UITextField *fieldSecretAccessKey;

@end
