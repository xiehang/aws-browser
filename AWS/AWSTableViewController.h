//
//  AWSTableViewController.h
//  AWS
//
//  Created by Hang Xie on 12-1-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AWSTableViewController : UITableViewController

@property (strong, nonatomic) NSMutableArray * items;

- (IBAction)rightButtonClicked:(id)sender;

@end
