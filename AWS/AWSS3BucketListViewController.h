//
//  AWSS3BucketListViewController.h
//  AWS
//
//  Created by Hang Xie on 12-1-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AWSTableViewController.h"

@interface AWSS3BucketListViewController : AWSTableViewController

@property (copy, nonatomic) NSDictionary * selection;
@property (weak, nonatomic) id delegate;
@property (weak, nonatomic) IBOutlet UITableView *myTableView;

@end
