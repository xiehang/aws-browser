//
//  AWSViewController.m
//  AWS
//
//  Created by Hang Xie on 12-1-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "AWSViewController.h"
#import "AWSConstants.h"

@implementation UIView (FindFirstResponder)
- (UIView *)findFirstResponder
{
    if (self.isFirstResponder)
    {
        return self;
    }

    for (UIView *subView in self.subviews)
    {
        UIView *firstResponder = [subView findFirstResponder];

        if (firstResponder != nil)
        {
            return firstResponder;
        }
    }

    return nil;
}
@end

@implementation AWSViewController

- (IBAction)backgroundTouched:(id)sender
{
    [[self.view findFirstResponder] resignFirstResponder];
}

- (IBAction)rightButtonClicked:(id)sender
{
    return;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithTitle:AWS_TITLE_RIGHT_BUTTON
                                              style:UIBarButtonItemStyleBordered
                                              target:self
                                              action:@selector(rightButtonClicked:)];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
}

@end
