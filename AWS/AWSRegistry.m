//
//  AWSRegistry.m
//  AWS
//
//  Created by Hang Xie on 12-1-27.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "AWSRegistry.h"

static AWSRegistry * sharedInstance = nil;

@implementation AWSRegistry

@synthesize dict;

- (id)init
{
    self = [super init];
    dict = [[NSMutableDictionary alloc] init];
    return self;
}

- (void)dealloc
{
    // Should never be called, but just here for clarity really.
}

+ (NSString *)registryFileName
{
    static NSString * _registryFileName = nil;

    if (_registryFileName == nil)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        _registryFileName = [[paths objectAtIndex:0] stringByAppendingPathComponent:AWS_REGISTRY_FILE_NAME];
    }
    return _registryFileName;
}

+ (const NSString *)REG_ACCESS_KEY
{
    return AWS_ACCESS_KEY_ID;
}

+ (const NSString *)REG_SECRET_KEY
{
    return AWS_SECRET_ACCESS_KEY;
}

+ (AWSRegistry *)instance
{
    @synchronized(self)
    {
        if (sharedInstance == nil)
        {
            sharedInstance = [[self alloc] init];
            if (sharedInstance != nil)
            {
                // need to load in data from file
                [AWSRegistry reload];
            }
        }
    }
    return sharedInstance;
}

+ (Boolean)save
{
    [sharedInstance.dict writeToFile:[AWSRegistry registryFileName] atomically:YES];
    return YES;
}

+ (Boolean)reload
{
    [sharedInstance.dict removeAllObjects];
    NSMutableDictionary * new_dict = [[NSMutableDictionary alloc] initWithContentsOfFile:[AWSRegistry registryFileName]];
    if (new_dict != nil)
    {
        sharedInstance.dict = new_dict;
    }
    return YES;
}

@end
