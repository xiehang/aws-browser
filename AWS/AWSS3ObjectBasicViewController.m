//
//  AWSS3ObjectBasicViewController.m
//  AWS
//
//  Created by Hang Xie on 12-1-31.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <AWSS3/AmazonS3Client.h>
#import "AWSS3ObjectBasicViewController.h"
#import "AWSRegistry.h"
#import "AWSS3Constants.h"

@implementation AWSS3ObjectBasicViewController

@synthesize selection;

@synthesize labelBucket;
@synthesize labelPath;
@synthesize labelName;
@synthesize labelSize;
@synthesize labelLastModified;
@synthesize labelETag;
@synthesize labelExpiryDate;
@synthesize labelLifecycleRule;

- (void)loadDetails
{
    NSString * bucketName = [selection objectForKey:AWS_DATA_KEY_BUCKET];
    NSString * objectName =[selection objectForKey:AWS_DATA_KEY_OBJECT];

    // divid objectName into path and file name
    NSMutableArray * paths = [[NSMutableArray alloc] initWithArray:[objectName pathComponents]];
    NSString * name = [paths lastObject];
    [paths removeLastObject];
    NSString * path = [AWS_S3_PATH_SEPARATOR stringByAppendingString:[paths componentsJoinedByString:AWS_S3_PATH_SEPARATOR]];

    NSString * accessKey = [[[AWSRegistry instance] dict] valueForKey:[AWSRegistry REG_ACCESS_KEY]];
    NSString * secretKey = [[[AWSRegistry instance] dict] valueForKey:[AWSRegistry REG_SECRET_KEY]];

    @try
    {
        // now let's load detail of the object
        AmazonS3Client * s3 = [[AmazonS3Client alloc]
                               initWithAccessKey:accessKey
                               withSecretKey:secretKey];

        S3GetObjectMetadataRequest * getObjectMetadataRequest = [[S3GetObjectMetadataRequest alloc] initWithKey:objectName withBucket:bucketName];
        S3GetObjectMetadataResponse * getObjectMetadataResponse = [s3 getObjectMetadata:getObjectMetadataRequest];

        // comma separated format
        NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];

        [labelBucket setText:bucketName];
        [labelPath setText:path];
        [labelName setText:name];
        [labelSize setText:[numberFormatter stringFromNumber:[NSNumber numberWithLongLong:[getObjectMetadataResponse contentLength]]]];

        [labelLastModified setText:[[getObjectMetadataResponse lastModified] stringWithRFC822Format]];
        [labelETag setText:[[getObjectMetadataResponse etag] stringByReplacingOccurrencesOfString:@"\"" withString:AWS_EMPTY_STRING]];

        // deal with expiration, by default there should be no expiration
        [labelExpiryDate setText:@"N/A"];
        [labelLifecycleRule setText:@"N/A"];
        NSString * expiriation = [getObjectMetadataResponse valueForHTTPHeaderField:@"x-amz-expiration"];
        if (expiriation != nil)
        {
            for (NSString * item in [expiriation componentsSeparatedByString:@"\", "])
            {
                NSArray * kvPair = [item componentsSeparatedByString:@"="];
                NSString * key = [kvPair objectAtIndex:0];
                NSString * value = [kvPair lastObject];
                if (([key caseInsensitiveCompare:@"expiry-date"] == NSOrderedSame)
                    && (value != nil) && ([value compare:@"\""] != NSOrderedSame))
                {
                    [labelExpiryDate setText:[value stringByReplacingOccurrencesOfString:@"\"" withString:AWS_EMPTY_STRING]];
                }
                else if (([key caseInsensitiveCompare:@"rule-id"] == NSOrderedSame)
                    && (value != nil) && ([value compare:@"\""] != NSOrderedSame))
                {
                    [labelLifecycleRule setText:[value stringByReplacingOccurrencesOfString:@"\"" withString:AWS_EMPTY_STRING]];
                }
            }
        }
    }
    @catch (AmazonClientException *exception)
    {
        UIAlertView * alert = [[UIAlertView alloc]
                               initWithTitle:@"AWS Exception"
                               message:[exception message]
                               delegate:self
                               cancelButtonTitle:@"OK"
                               otherButtonTitles:nil];
        [alert show];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];

    // we don't need right button for this page
    self.navigationItem.rightBarButtonItem = nil;

    // load objects from this bucket
    [self loadDetails];
}

- (void)viewDidUnload
{
    [self setLabelBucket:nil];
    [self setLabelPath:nil];
    [self setLabelName:nil];
    [self setLabelSize:nil];
    [self setLabelLastModified:nil];
    [self setLabelETag:nil];
    [self setLabelExpiryDate:nil];
    [self setLabelLifecycleRule:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 9;
}

@end
