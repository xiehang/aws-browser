//
//  AWSEC2InstanceListViewController.m
//  AWS
//
//  Created by Hang Xie on 12-2-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <AWSEC2/AmazonEC2Client.h>

#import "AWSAccessKeyViewController.h"
#import "AWSEC2InstanceListViewController.h"
#import "AWSEC2Instance.h"
#import "AWSRegistry.h"
#import "AWSEC2Constants.h"

@implementation AWSEC2InstanceListViewController

@synthesize selection;
@synthesize delegate;
@synthesize myTableView;

NSMutableDictionary * instances = nil;

- (void)loadInstances
{
    // clean up existing items
    if (instances == nil)
    {
        instances = [[NSMutableDictionary alloc] init];
    }
    else
    {
        [instances removeAllObjects];
    }

    // get credentials - credentials should be useable as we came from bucket listing page
    NSString * accessKey = [[[AWSRegistry instance] dict] valueForKey:[AWSRegistry REG_ACCESS_KEY]];
    NSString * secretKey = [[[AWSRegistry instance] dict] valueForKey:[AWSRegistry REG_SECRET_KEY]];
    
    // make sure credentials are useable
    if ((accessKey == nil) || (secretKey == nil))
    {
        // no credentials defined, do nothing
        return;
    }

    @try
    {
        // now let's load objects in the bucket
        AmazonEC2Client * ec2 = [[AmazonEC2Client alloc]
                                 initWithAccessKey:accessKey
                                 withSecretKey:secretKey];

        // get all regions
        EC2DescribeRegionsRequest * describeRegionsRequest = [[EC2DescribeRegionsRequest alloc] init];
        EC2DescribeRegionsResponse * describeRegionsResponse =[[EC2DescribeRegionsResponse alloc] init];
        describeRegionsResponse = [ec2 describeRegions:describeRegionsRequest];

        EC2DescribeInstancesRequest * describeInstancesRequest = [[EC2DescribeInstancesRequest alloc] init];
        EC2DescribeInstancesResponse * describeInstanceReponse = [[EC2DescribeInstancesResponse alloc] init];

        for (EC2Region * region in [describeRegionsResponse regions])
        {
            [ec2 setEndpoint:[@"http://" stringByAppendingString:[region endpoint]]];
            describeInstanceReponse = [ec2 describeInstances:describeInstancesRequest];
            for (EC2Reservation * reservation in [describeInstanceReponse reservations])
            {
                for (EC2Instance * ec2Instance in [reservation instances])
                {
                    AWSEC2Instance * instance = [[AWSEC2Instance alloc] initWithEC2Instance:ec2Instance];
                    [instance setEndpoint:[region endpoint]];
                    [instances setObject:instance forKey:instance.instanceId];
                }
            }
        }
    }
    @catch (AmazonClientException *exception)
    {
        UIAlertView * alert = [[UIAlertView alloc]
                               initWithTitle:@"AWS Exception"
                               message:[exception message]
                               delegate:self
                               cancelButtonTitle:AWS_TITLE_OK
                               otherButtonTitles:nil];
        [alert show];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadInstances];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
        {
            [self.navigationController popViewControllerAnimated:YES];
            break;
        }
        default:
        {
            // user choose to edit credentials now
            AWSAccessKeyViewController * accessKeyView = [self.storyboard instantiateViewControllerWithIdentifier:AWS_VIEW_KEY_ACCESS_KEY];
            [self.navigationController pushViewController:accessKeyView animated:YES];
            break;
        }
    }
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    instances = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    // if there is no credentials defined, we need to go back to configuration page
    // get credentials
    NSString * accessKey = [[[AWSRegistry instance] dict] valueForKey:[AWSRegistry REG_ACCESS_KEY]];
    NSString * secretKey = [[[AWSRegistry instance] dict] valueForKey:[AWSRegistry REG_SECRET_KEY]];

    // make sure credentials are useable
    if ((accessKey == nil) || (secretKey == nil))
    {
        // tell the use we need keys
        UIAlertView * alert = [[UIAlertView alloc]
                               initWithTitle:AWS_TITLE_NEED_CREDENTIALS
                               message:AWS_MESSAGE_NEED_CREDENTIALS
                               delegate:self
                               cancelButtonTitle:AWS_TITLE_NO
                               otherButtonTitles:AWS_TITLE_YES, nil];
        [alert show];

        return;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [instances count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"itemCell";

    AWSEC2Instance * instance = [instances objectForKey:[[instances allKeys] objectAtIndex:indexPath.row]];
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    // Configure the cell...
    UILabel * cellId = (UILabel *)[cell viewWithTag:1];
    UILabel * cellDescription = (UILabel *)[cell viewWithTag:2];

    [cellId setText:instance.instanceId];
    [cellDescription setText:[[NSString alloc] initWithFormat:@"%s, %s, %s",
                              [[instance.state name] UTF8String],
                              [[instance.placement availabilityZone] UTF8String],
                              [instance.instanceType UTF8String]
                              ]];

    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController * destination = segue.destinationViewController;

    if ([destination respondsToSelector:@selector(setDelegate:)])
    {
        [destination setValue:self forKey:NSLocalizedString(@"delegate", @"Key for setting subview controller's behavior, DON'T CHANGE")];
    }

    if ([destination respondsToSelector:@selector(setSelection:)])
    {
        // prepare selection info
        NSIndexPath * indexPath = [self.tableView indexPathForCell:sender];
        id instanceName = [[instances allKeys] objectAtIndex:indexPath.row];
        id endpoint = [[instances objectForKey:instanceName] endpoint];
        NSDictionary * objectAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                           indexPath, NSLocalizedString(@"indexPath", @"Key of for a subview of TableView to hold currently selected cell"),
                                           instanceName, AWS_DATA_KEY_INSTANCE,
                                           endpoint, AWS_DATA_KEY_ENDPOINT,
                                           nil];
        [destination setValue:objectAttributes forKey:NSLocalizedString(@"selection", @"Key for setting subview controller's initial data set")];
    }
}

@end
