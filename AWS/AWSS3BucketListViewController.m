//
//  AWSS3BucketListViewController.m
//  AWS
//
//  Created by Hang Xie on 12-1-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <AWSS3/AmazonS3Client.h>

#import "AWSS3BucketListViewController.h"
#import "AWSAccessKeyViewController.h"
#import "AWSRegistry.h"
#import "AWSS3Constants.h"

@implementation AWSS3BucketListViewController

- (void)loadBuckets
{
    // clean up existing items
    if (self.items == nil)
    {
        self.items = [[NSMutableArray alloc] init];
    }
    else
    {
        [self.items removeAllObjects];
    }

    // get credentials
    NSString * accessKey = [[[AWSRegistry instance] dict] valueForKey:[AWSRegistry REG_ACCESS_KEY]];
    NSString * secretKey = [[[AWSRegistry instance] dict] valueForKey:[AWSRegistry REG_SECRET_KEY]];

    // make sure credentials are useable
    if ((accessKey == nil) || (secretKey == nil))
    {
        // no credentials defined, do nothing
        return;
    }

    @try {
        // now let's load stuffs from AWS
        AmazonS3Client * s3 = [[AmazonS3Client alloc]
                               initWithAccessKey:accessKey
                               withSecretKey:secretKey];
        NSArray * buckets = [s3 listBuckets];
        if (buckets == nil)
        {
            // something weird happened, note that this should not be exception from AWS
            UIAlertView * alert = [[UIAlertView alloc]
                                   initWithTitle:AWS_TITLE_UNKNOWN_ERROR
                                   message:AWS_MESSAGE_UNKNOWN_ERROR
                                   delegate:self
                                   cancelButtonTitle:AWS_TITLE_OK
                                   otherButtonTitles:nil];
            [alert show];
            return;
        }

        for (S3Bucket * bucket in buckets)
        {
            [self.items addObject:[bucket name]];
        }

        // sort buckets by their name
        [self.items sortUsingSelector:@selector(compare:)];
    }
    @catch (AmazonClientException *exception)
    {
        UIAlertView * alert = [[UIAlertView alloc]
                               initWithTitle:NSLocalizedString(@"AWS Exception", @"Title of alert window telling AWS error")
                               message:[exception message]
                               delegate:self
                               cancelButtonTitle:AWS_TITLE_OK
                               otherButtonTitles:nil];
        [alert show];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];

    // try to load buckets
    [self loadBuckets];

    // we don't need right button for this page
    self.navigationItem.rightBarButtonItem = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    // if there is no credentials defined, we need to go back to configuration page
    // get credentials
    NSString * accessKey = [[[AWSRegistry instance] dict] valueForKey:[AWSRegistry REG_ACCESS_KEY]];
    NSString * secretKey = [[[AWSRegistry instance] dict] valueForKey:[AWSRegistry REG_SECRET_KEY]];

    // make sure credentials are useable
    if ((accessKey == nil) || (secretKey == nil))
    {
        // tell the user we need keys
        UIAlertView * alert = [[UIAlertView alloc]
                               initWithTitle:AWS_TITLE_NEED_CREDENTIALS
                               message:AWS_MESSAGE_NEED_CREDENTIALS
                               delegate:self
                               cancelButtonTitle:AWS_TITLE_NO
                               otherButtonTitles:AWS_TITLE_YES, nil];
        [alert show];
        
        return;
    }
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
        {
            // navgate to up level window as user chose "No"
            [self.navigationController popViewControllerAnimated:YES];
            break;
        }
        default:
        {
            // user choose to edit credentials now
            AWSAccessKeyViewController * accessKeyView = [self.storyboard instantiateViewControllerWithIdentifier:AWS_VIEW_KEY_ACCESS_KEY];
            [self.navigationController pushViewController:accessKeyView animated:YES];
            break;
        }
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController * destination = segue.destinationViewController;

    if ([destination respondsToSelector:@selector(setDelegate:)])
    {
        [destination setValue:self forKey:NSLocalizedString(@"delegate", @"Key for setting subview controller's behavior, DON'T CHANGE")];
    }
    if ([destination respondsToSelector:@selector(setSelection:)])
    {
        // prepare selection info
        NSIndexPath * indexPath = [self.tableView indexPathForCell:sender];
        id bucketName = [self.items objectAtIndex:indexPath.row];
        NSDictionary * objectAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                           indexPath, NSLocalizedString(@"indexPath", @"Key of for a subview of TableView to hold currently selected cell"),
                                           bucketName, AWS_DATA_KEY_BUCKET,
                                           AWS_EMPTY_STRING, AWS_DATA_KEY_PREFIX,
                                           nil];
        [destination setValue:objectAttributes forKey:NSLocalizedString(@"selection", @"Key for setting subview controller's initial data set")];
    }
}

@end
