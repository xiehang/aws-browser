//
//  AWSMainViewController.h
//  AWS
//
//  Created by Hang Xie on 12-2-15.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AWSMainViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UILabel *labelS3;
@property (weak, nonatomic) IBOutlet UILabel *labelEC2;
@property (weak, nonatomic) IBOutlet UILabel *labelDynamoDB;
@property (weak, nonatomic) IBOutlet UILabel *labelSimpleDB;
@property (weak, nonatomic) IBOutlet UILabel *labelRDS;
@property (weak, nonatomic) IBOutlet UILabel *labelConfiguration;

@end
