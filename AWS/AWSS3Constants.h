//
//  AWSS3Constants.h
//  AWS
//
//  Created by Hang Xie on 12-2-2.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#ifndef AWS_AWSS3UIProtocolConstants_h
#define AWS_AWSS3UIProtocolConstants_h

#import "AWSConstants.h"

#define AWS_S3_PATH_SEPARATOR @"/"

// ids for view controllers
#define AWS_VIEW_S3_OBJECT_BASIC @"UIViewController-J3c-Yl-fa4"

// keys for transferring data between views
#define AWS_DATA_KEY_BUCKET @"bucketName"
#define AWS_DATA_KEY_PREFIX @"pathPrefix"
#define AWS_DATA_KEY_OBJECT @"objectName"

#endif
